﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Pokemon.BusinessLogic
{
  public class CharacterDTO
  {
    //field
    private static int characterCount = 0;

    //backing field
    private string _CharacterName = default(string);

    //properties
    public string CharacterName
    {
      get
      {
        return _CharacterName;
      }

      set
      {
        var regex = new Regex(@"^a$", RegexOptions.IgnoreCase);

        if (regex.IsMatch(value))
        {
          _CharacterName = value;
        }

      }
    }

    public decimal Height { get; set; }

    public decimal Weight { get; set; }
  }
}
