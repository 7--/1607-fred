﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon.DataAccess
{
  public class EFData
  {
    private PokemonDBEntities db = new PokemonDBEntities();

    public void GetAllPokemon()
    {
      var pokemon = db.Characters.ToList();

      foreach (var item in pokemon)
      {
        Console.WriteLine(item.Name);
      }
    }

    public bool InsertPokemon(Character c)
    {
      try
      {
        db.Characters.Add(c);
        db.SaveChanges();

        return true;
      }
      catch (Exception ex)
      {
        //throw new Exception(ex.Message, ex);
        return false;
      }
      finally
      {
        //always runs.
      }
    }

    public bool ChangePokemon(Character c, EntityState s)
    {
      try
      {
        var entry = db.Entry(c);
        entry.State = s;
        db.SaveChanges();

        return true;
      }
      catch (Exception ex)
      {
        return false;
      }
    }

    public List<vw_pokemon_vitals> GetAllPokemonVitals()
    {
      var x = db.vw_pokemon_vitals.ToList();
      return x;
    }

    [DbFunction("PokemonDBModel.Store", "fn_getpokemon_data")]
    public static string GetPokemonData(int? pid)
    {
      throw new NotSupportedException();
    }

    public bool UpdatePokemonHeight(int pid, decimal height)
    {
      return db.sp_updatepokemonheight(pid, height) != 0;
    }

    public List<Character> LazyLoading()
    {
      var query = from c in db.Characters
                  select c;

      var C = new Character();

      db.Characters.Add(new Character()
      {
        Name = "Sunday",
        Height = 3.45M,
        Weight = 100.25M,
        DateModified = DateTime.Now,
        DocumentGuid = Guid.NewGuid(),
        Active = true
      });

      db.SaveChanges();

      return query.ToList();
    }
  }
}
