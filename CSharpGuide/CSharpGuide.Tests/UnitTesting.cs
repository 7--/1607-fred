﻿using CSharpGuide.ConsoleClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CSharpGuide.Tests
{
  public class UnitTesting
  {
    [Fact]
    public void DistanceIsCorrect()
    {
      var expected = 2D;
      double actual;
      var p = new Point(2,0);

      actual = p.DistanceToOrigin();

      Assert.Equal(expected, actual);
    }

    [Fact]
    public void DistanceNotCorrect()
    {
      var expected = 4D;
      double actual;
      var p = new Point(2, 0);

      actual = p.DistanceToOrigin();

      Assert.NotEqual(expected, actual);
    }
  }
}
