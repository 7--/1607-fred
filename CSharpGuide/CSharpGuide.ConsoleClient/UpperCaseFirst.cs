﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpGuide.ConsoleClient
{
  static class UpperCaseFirst
  {
    public static string Capitalize(this string s)
    {
      string tempString;
      var t = s[0].ToString().ToUpper();
      tempString = t + s.Substring(1);

      return tempString;
    }
  }
}
