﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpGuide.ConsoleClient
{
  public class Square : Rectangle
  {
    public override double Width
    {
      get
      {
        return Length;
      }

      set
      {
        Length = value;
      }
    }
  }
}
