﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CSharpGuide.ConsoleClient.Serializista
{
  public class Contact
  {
    public void Add()
    {
      Console.Write("enter your name: ");
      var name = Console.ReadLine();

      Console.WriteLine();

      Console.Write("enter your addresses: ");
      var addresses = Console.ReadLine().Split(';');

      var p = new Person();

      p.Firstname = name.Split()[0];
      p.Lastname = name.Split()[1];
      p.Addresses = new List<Address>();

      foreach (var item in addresses.ToList())
      {
        p.Addresses.Add(new Address()
        {
          City = item.Split(',')[0]
          ,State = item.Split(',')[1]
        });
      }

      WriteToFileXml(p);
    }

    private void WriteToFile(Person p)
    {
      var path = @"c:\revature\contact.txt";

      using (var file = File.AppendText(path))
      {
        file.Write(string.Format("{0} {1} - ", p.Firstname, p.Lastname));

        foreach (var item in p.Addresses)
        {
          file.Write(string.Format("{0},{1}; ", item.City, item.State));
        }

        file.WriteLine();
      }
    }

    

    private void WriteToFileXml(Person p)
    {
      var path = @"c:\revature\contact.xml";
      var xml = new XmlSerializer(typeof(Person));
      var writer = new StreamWriter(path);

      xml.Serialize(writer, p);
      writer.Close();
    }
    private void WriteCustomXml()
    {
      var xmlDoc = new XmlDocument();
      var name = xmlDoc.CreateElement("firstname");
      name.InnerText = "fred";
      xmlDoc.AppendChild(name);
     
    }
    //Notice it ignores addresses
    public void WriteXmlPeople2(List<Person> p)
    {
      foreach (var item in p)
      {
        WriteToFile(item);
      }
    }
    public void WriteXmlPeople(List<Person> p)
    {
      var xmlDoc = new XmlDocument();
      foreach (var item in p)
      {
        var PersonLabel = xmlDoc.CreateElement("Person");
        var FirstNamLabel = xmlDoc.CreateElement("firstname");
        FirstNamLabel.InnerText = "fred";
        PersonLabel.AppendChild(PersonLabel);

      }
    }
  }
}
