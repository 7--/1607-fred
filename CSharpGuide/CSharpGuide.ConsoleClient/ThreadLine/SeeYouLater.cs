﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CSharpGuide.ConsoleClient.ThreadLine
{
  public class SeeYouLater
  {
    private void ConsoleOutput(string stuff, int count)
    {
      for (int i = 0; i < count; i += 1)
      {
        Console.Write(stuff);
      }
    }

    public void WorkWithThread()
    {
      var t1 = new Thread(() => { ConsoleOutput("A", 100); });
      var t2 = new Thread(() => { ConsoleOutput("B", 100); });
      var t3 = new Thread(() => { ConsoleOutput("C", 100); });

      ConsoleOutput("D", 100);

      t1.Start();
      t2.Start();
      t3.Start();

      t1.Join();
      t2.Join();
      t3.Join();
    }

    public void WorkWithTask()
    {
      var t1 = new Task(() => { ConsoleOutput("A", 10000); });
      var t2 = new Task(() => { ConsoleOutput("B", 10000); });
      var t3 = new Task(() => { ConsoleOutput("C", 10000); });

      t1.Start();
      t2.Start();
      t3.Start();

      ConsoleOutput("D", 10000);

      Task.WaitAll(new Task[] { t1, t2, t3 }, 1);
    }

    public Task WorkWithAsync()
    {
      return Task.Run(() => { Thread.Sleep(3000); Console.WriteLine("Task is Calling"); });
    }
    public void WorkWithThread2()
    {
      var t1 = new Thread(() => { ConsoleOutput("A", 100); new Thread(() => { ConsoleOutput("B", 100); new Thread(() => { ConsoleOutput("C", 100); new Thread(() => { ConsoleOutput("D", 100); new Thread(() => { ConsoleOutput("E", 100); }).Start(); }).Start(); }).Start(); }).Start(); });
      
      var t2 = new Thread(() => { t1.Join(); 
        ConsoleOutput("222222222222222222222222222222222",3); });
      t1.Start();

      t2.Start();
      t1.Join();
    }
    public void WorkWithThread3()
    {
      int a=5;
      int b;
      int c;
      var t1 = new Thread(() => {
        //Thread.Sleep(100);
        a = 1;
        //ConsoleOutput("111111111111111111111111111111111", 3);
        
        Thread.Sleep(100);
      });
      var t2 = new Thread(() => {
        Thread.Sleep(100);
        a = 2;
        //ConsoleOutput("222222222222222222222222222222222", 3);
        Thread.Sleep(100);
      });
      var t3 = new Thread(() => {
        Thread.Sleep(100);
        a = 3;
        //ConsoleOutput("333333333333333333333333333333333", 3);
        Thread.Sleep(100);
      });
      t1.Start();
      t2.Start();
      t3.Start();
      t1.Join();
      Console.WriteLine(a);

      t2.Join();
      Console.WriteLine(a);
      t3.Join();
      Console.WriteLine(a);

      


      //Console.WriteLine(a);
      //t2.Join();
      //t3.Join();

    }
  }
}
