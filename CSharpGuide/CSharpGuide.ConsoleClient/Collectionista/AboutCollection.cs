﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpGuide.ConsoleClient.Collectionista
{
  class AboutCollection
  {
    public void ArrayGoesDown()
    {
      int[] arr1 = new int[5];
      var arr2 = new int[] { 1, 2, 3, 4, 5 };

      //read
      var x = arr2[2]; //3

      //write
      arr2[5] = 10;

      //x-d
      int[,] arr3 = new int[2, 2];
      int[,] arr4 = new int[,] { { 1, 2 }, { 3, 4 } };

      //read and write
      var y = arr4[1, 0]; //3
      arr4[0, 1] = 10;

      //jagged array
      int[][] arr5 = new int[2][];
      int[][] arr6 = new int[][] 
      {
        new int[] { 1, 2 },
        new int[] { 3, 4, 5 }
      };

      //read and write
      var z = arr6[0][1]; //2
      arr6[1][1] = 10;
    }

    public void ListIsAwesome()
    {
      var ls1 = new List<int>();
      var ls2 = new List<int> { 1, 2, 3, 4, 5 };

      ls2[10] = 10;
      ls2.Add(10);
      ls2.Remove(10);
      ls2.RemoveAll(i => i == 10);

      ls2.ElementAt(10);
      ls2.FirstOrDefault();
    }

    public void DisctionaryToTheRescue()
    {
      var dt1 = new Dictionary<string, int>();
      var dt2 = new Dictionary<string, int>
      {
        {"1", 1 },
        {"2", 2 }
      };

      dt2["2"] = 3;
      dt2.Add("2", 4);

      if (!dt2.ContainsKey("2"))
      {
        dt2.Add("2", 5);
      }
    }

    public bool Check10(int i)
    {
      if (i == 10)
      {
        return true;
      }

      return false;
    }
  }
}
