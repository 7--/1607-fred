﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpGuide.ConsoleClient
{
  public struct Point
  {
    public int X { get; set; }
    public int Y { get; set; }

    public Point(int x, int y)
    {
      X = x;
      Y = y;
    }

    public double DistanceToOrigin()
    {
      return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
    }

    public double DistanceToOrigin(int x = 1, int y = 1)
    {
      return Math.Sqrt((Math.Pow(X, 2) - Math.Pow(x, 2)) + (Math.Pow(Y, 2) - Math.Pow(y, 2)));
    }
  }
}
